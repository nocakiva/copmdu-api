﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using COPMDU.Domain;
using COPMDU.Domain.Entity;
using COPMDU.Domain.Notification;
using COPMDU.Infrastructure;
using COPMDU.Infrastructure.Interface;
using COPMDU.Infrastructure.PageModel;
using COPMDU.Infrastructure.Util;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;


namespace COPMDU.Api.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class TicketController : Controller
    {
        private readonly ITicketRepository _ticketRepository;
        private readonly ISignalRepository _signalRepository;
        private readonly INotificationRepository _notificationRepository;
        private readonly IFrontMdu _frontMdu;
        private readonly IResolutionRepository _resolutionRepository;
        private readonly IConfiguration _config;
        private readonly ILogger _logger;
        private readonly ILogRepository _logRepository;
        private readonly IHttpContextAccessor _context;
        private readonly IUserRepository _user;
        private readonly Messager _messager;

        private string Username => _config["Credentials:NewMonitor:User"];
        private string Password => _config["Credentials:NewMonitor:Password"];
        private int TicketTolerance => int.Parse(_config["CopMdu:TicketCloseToleranceMinutes"]);


        public TicketController(
                                ITicketRepository ticketRepository,
                                ISignalRepository signalRepository,
                                INotificationRepository notificationRepository,
                                IFrontMdu frontMdu,
                                IResolutionRepository resolutionRepository,
                                IConfiguration configuration,
                                ILogger<TicketController> logger,
                                ILogRepository logRepository,
                                IUserRepository user,
                                IHttpContextAccessor context
                                )
        {
            _ticketRepository = ticketRepository;
            _signalRepository = signalRepository;
            _notificationRepository = notificationRepository;
            _frontMdu = frontMdu;
            _resolutionRepository = resolutionRepository;
            _config = configuration;
            _logger = logger;
            _logRepository = logRepository;
            _context = context;
            _user = user;
            _messager =  new Messager(_config);
        }

        [HttpGet]
        public IEnumerable<Outage> Get()
        {
            try
            {
                _logger.LogInformation("Get tickets of technician");

                var id = _context.CurrentUser();
                _logRepository.Add(new Log { UserId = id, Source = "TicketController", Message = $"Coletado outages do sistema" });

                // TODO: Integrate login 
                return _ticketRepository.GetAllTechnician(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logRepository.RecordException(ex);
                throw;
            }
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public Outage Get(int id)
        {
            try
            {
                this.User.FindFirstValue(ClaimTypes.NameIdentifier);
                _logger.LogInformation("Get ticket by id");
                return _ticketRepository.Get(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logRepository.RecordException(ex);
                throw;
            }
        }

        [AllowAnonymous]
        [HttpPost("NotificationClose")]
        public async Task<bool> NotificationClose([FromBody]TicketModel ticketData)
        {
            var outage = _ticketRepository.Get(ticketData.Ticket);
            var resolutionAtlas = _resolutionRepository.GetById(ticketData.IdResolution);

            return await _notificationRepository.Close(
                outage.Notification,
                resolutionAtlas.AtlasId,
                ticketData.Description,
                outage.CityId ?? 0) == CloseStatus.ClosedSuccessfully;
        }

        private async Task<bool> TicketStillOpen(int ticket)
        {
            await _ticketRepository.Authenticate(Username, Password);
            var detail = await _ticketRepository.GetTicketDetail(ticket);
            if (detail != null)
                return detail.FinalDate == null || detail.FinalDate == "ND" || detail.FinalDate == "";
            return false;
        }

        [HttpPost("Close")]
        public async Task<Ticket> Close([FromBody]TicketModel ticketData)
        {
            _logger.LogInformation("Close ticket");

            var ticket = new Ticket();

            try
            {
                if (!await TicketStillOpen(ticketData.Ticket))
                    return new Ticket
                    {
                        ValidStatus = false,
                        Status = $"Ticket {ticketData.Ticket} já está finalizado."
                    };
                    

                ticketData.Description = AddUserInfo(ticketData.Description, ticketData.ClosedById);
                var outage = _ticketRepository.Get(ticketData.Ticket);
                if (outage == null)
                    return new Ticket
                    {
                        ValidStatus = false,
                        Status = $"Ticket {ticketData.Ticket} não foi encontrado no sistema."
                    };

                if (outage.Notification > 0)
                {
                    var resolutionAtlas = _resolutionRepository.GetById(ticketData.IdResolution);
                    if (resolutionAtlas == null)
                        throw new Exception($"Resolução com id {ticketData.IdResolution} não encontrada.");
                    var notificationResult = await _notificationRepository.Close(
                        outage.Notification,
                        resolutionAtlas.AtlasId,
                        ticketData.Description,
                        outage.CityId ?? 0);

                    if (notificationResult == CloseStatus.ClosedSuccessfully)
                    {
                        ticket = await CloseTicket(ticketData);
                        if (ticket.ValidStatus){
                            await _ticketRepository.CloseDb(ticketData.Ticket, ticketData.ClosedById);
                            SendClosedMessage(ticketData.ClosedById, ticketData.Ticket);
                        }
                    }
                    else
                    {
                        ticket.ValidStatus = notificationResult == CloseStatus.ClosedSuccessfully;
                        ticket.Status = notificationResult == CloseStatus.ClosedSuccessfully ? "" : $"Não conseguiu finalizar a notificação: {ticket.Notification}";
                    }
                }
                else
                {
                    ticket.ValidStatus = false;
                    ticket.Status = $"Não conseguiu finalizar a notificação, não encontrou nenhuma notificação vinculada ao ticket {ticketData.Ticket}";
                }

            }
            catch (Exception ex)
            {
                _logger.LogError($"Falha ao fechar o ticket: {ex.Message}", ticketData.Ticket);
                _logRepository.RecordException(ex);
                ticket.Status = ex.Message;
            }
            _logger.LogInformation($"Fechamento do ticket finalizado {ticket.Status}");
            return ticket;
        }

        private void SendClosedMessage(int userId, int ticket)
        {
            try
            {
                var user = _user.Get(userId);
                var to = _config["Messages:TicketClosed:To"];
                var body = string.Format(_config["Messages:TicketClosed:Message"], ticket, user.Name, user.Username);
                var subject = string.Format(_config["Messages:TicketClosed:Subject"], user.Name, ticket);
                
                _messager.Send(to, subject, body);
            }
            catch (System.Exception)
            {
                //FUNCIONALIDADE EM TESTES DELETE ME LATER
            }
        }

        [AllowAnonymous]
        [HttpGet("GetClosed")]
        public IEnumerable<Outage> GetClosed()
        {
            try
            {
                _logger.LogInformation("Get closed tickets of technician");

                var id = _context.CurrentUser();
                _logRepository.Add(new Log { UserId = id, Source = "TicketController", Message = $"Coletado closed outages" });

                // TODO: Integrate login 
                return _ticketRepository.GetAllClosed(id);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                _logRepository.RecordException(ex);
                throw;
            }
        }

        #region app unused methods

        [HttpGet("GetTicketsNM")]
        public async Task<List<TicketResponse>> GetTicketsNM()
        {
            try {
                return await _ticketRepository.GetTicketsNM();
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }

        [HttpGet("GetTicketDetail/{id}")]
        public async Task<OutageResponse> GetTicketDetail(int id)
        {
            try
            {
                await _ticketRepository.Authenticate(Username, Password);
                return await _ticketRepository.GetTicketDetail(id);
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }

        [AllowAnonymous]
        [HttpGet("GetCells/{node}/{cityId}")]
        public async Task<List<Cell>> GetCells(string node, int cityId) => 
            await _notificationRepository.GetCells(node, cityId);


        [AllowAnonymous]
        [HttpGet("GetOccurrences/{node}/{cell}/{cityId}")]
        public async Task<List<CitOccurrence>> GetOccurrences(string node, string cell, int cityId) =>
            await _notificationRepository.GetOccurrences(node, cell, cityId);
               
        [HttpGet("GetServiceOrders")]
        public async Task<List<ServiceOrder>> GetServiceOrders()
        {
            try
            {
                return await _ticketRepository.GetServiceOrders();
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }

        [HttpGet("LoadNewOutage")]
        public async Task<bool> LoadNewOutage()
        {
            try
            {
                await _ticketRepository.Authenticate(Username, Password);
                return await _ticketRepository.LoadNewOutage();
            }
            catch (Exception ex)
            {
                _logRepository.RecordException(ex);
                throw;
            }
        }
        
        private async Task CheckSignalIfNecessary(Outage outage)
        {
            if (outage.SignalExpiration == null || DateTime.Now > outage.SignalExpiration)
                await _signalRepository.GetSignal(outage);
        }


        private DateTime GetClosingTime(DateTime sla)
        {
            var time = DateTime.Now;
            if (sla < time && time < sla.AddMinutes(TicketTolerance))
                time = time.AddMinutes(-TicketTolerance);
            return time;
        }

        private string AddUserInfo(string obs, int idUser)
        {
            var user = _user.Get(idUser);
            var name = user.Name;
            if (user.Name != user.Username)
                name += $" ({user.Username})";
            return $"{obs} {name}";
        }

        private async Task<Ticket> CloseTicket(TicketModel ticketData)
        {
            var ticket = new Ticket();

            // User
            var user = await _ticketRepository.Authenticate(Username, Password);
            if(user == null)
            {
                ticket.Status = $"Não foi possível autenticar o usuário {Username} no New Monitor";
                return ticket;
            }

            // Ticket
            var outage = _ticketRepository.Get(ticketData.Ticket);
            if (outage == null)
            {
                ticket.ValidStatus = false;
                ticket.Status = $"Não encontrado ticket: {ticketData.Ticket}";
                return ticket;
            }
            ticket.Id = outage.Id;
            ticket.ClosedById = ticketData.ClosedById;
            ticket.Contract = outage.Contract;
            ticket.Notification = outage.Notification;
            ticket.IdResolution = ticketData.IdResolution;
            ticket.Description = ticketData.Description;

            // Signal
            var cid = outage.City.Id;
            ticket.Cid = cid;

            await CheckSignalIfNecessary(outage);
            var statusSignal = outage.ValidSignal ?? false;

            // Attach evidence
            var htmlContent = _signalRepository.GetHtml(outage.Signals);
            var fileData = _signalRepository.GetPdf(htmlContent);
            await _ticketRepository.AttachEvidence(ticket.Id, fileData);

            if (statusSignal)
            {
                var closeTime = GetClosingTime(outage.Sla);
                ticket.AffectedChannels = 0;
                var close = await _ticketRepository.Close(ticket.Id, ticket.IdResolution, ticket.Description, closeTime, ticketData.ClosedById);
                ticket.ValidStatus = close == CloseStatus.ClosedSuccessfully;
                
                if (close == CloseStatus.AlreadyClosed)
                    ticket.Status = "Ticket já se encontrava finalizado.";
                else
                    ticket.Status = close == CloseStatus.ClosedSuccessfully ? "" : $"Não conseguiu finalizar o ticket {ticket.Id} no New Monitor.";
            }
            else
            {
                ticket.ValidStatus = false;
                ticket.Status = "Nível do sinal abaixo do minímo necessário para fechamento";
            }

            return ticket;
        }

        #endregion
    }

    public class TicketModel
    {
        public int Ticket { get; set; }
        public int IdResolution { get; set; }
        public string Description { get; set; }
        public int ClosedById { get; set; }
    }
}
