FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-stretch-slim AS base
WORKDIR /app
EXPOSE 80

FROM mcr.microsoft.com/dotnet/core/sdk:2.2-stretch AS build
WORKDIR /src
COPY ["COPMDU.Api/COPMDU.Api.csproj", "COPMDU.Api/"]
COPY ["COPMDU.Domain/COPMDU.Domain.csproj", "COPMDU.Domain/"]
COPY ["COPMDU.Infrastructure/COPMDU.Infrastructure.csproj", "COPMDU.Infrastructure/"]
RUN dotnet restore "COPMDU.Api/COPMDU.Api.csproj"
COPY . .
WORKDIR "/src/COPMDU.Api"
RUN dotnet build "COPMDU.Api.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "COPMDU.Api.csproj" -c Release -o /app/publish

# install System.Drawing native dependencies
RUN apt-get update \
    && apt-get install -y --allow-unauthenticated \
   		libgdiplus \
#         libc6-dev \
#         libgdiplus \
#         libx11-dev \
     && rm -rf /var/lib/apt/lists/*

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "COPMDU.Api.dll"]