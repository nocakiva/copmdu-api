﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace COPMDU.Infrastructure.Migrations
{
    public partial class ResolutionActive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "Resolution",
                nullable: false,
                defaultValue: false);

            migrationBuilder.UpdateData(
                table: "Resolution",
                keyColumn: "Id",
                keyValue: 2959,
                column: "Active",
                value: true);

            migrationBuilder.UpdateData(
                table: "Resolution",
                keyColumn: "Id",
                keyValue: 2965,
                column: "Active",
                value: true);

            migrationBuilder.UpdateData(
                table: "Resolution",
                keyColumn: "Id",
                keyValue: 2967,
                column: "Active",
                value: true);

            migrationBuilder.UpdateData(
                table: "Resolution",
                keyColumn: "Id",
                keyValue: 2968,
                column: "Active",
                value: true);

            migrationBuilder.UpdateData(
                table: "Resolution",
                keyColumn: "Id",
                keyValue: 3063,
                column: "Active",
                value: true);

            migrationBuilder.UpdateData(
                table: "Resolution",
                keyColumn: "Id",
                keyValue: 3064,
                column: "Active",
                value: true);

            migrationBuilder.UpdateData(
                table: "Resolution",
                keyColumn: "Id",
                keyValue: 5404,
                column: "Active",
                value: true);

            migrationBuilder.UpdateData(
                table: "Resolution",
                keyColumn: "Id",
                keyValue: 5423,
                column: "Active",
                value: true);

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 2,
                column: "Password",
                value: "B6ijwn0u");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 3,
                column: "Password",
                value: "sEyapfj9");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 4,
                column: "Password",
                value: "aLPjv5uv");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 5,
                column: "Password",
                value: "hOtfhl6s");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "Resolution");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 2,
                column: "Password",
                value: "xdF6KJqa");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 3,
                column: "Password",
                value: "elsnuWSE");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 4,
                column: "Password",
                value: "3icGLCar");

            migrationBuilder.UpdateData(
                table: "User",
                keyColumn: "Id",
                keyValue: 5,
                column: "Password",
                value: "xlz47yhI");
        }
    }
}
