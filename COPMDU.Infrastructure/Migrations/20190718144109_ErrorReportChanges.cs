﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace COPMDU.Infrastructure.Migrations
{
    public partial class ErrorReportChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserErrorNotification");

            migrationBuilder.AddColumn<bool>(
                name: "General",
                table: "SystemAlert",
                nullable: false,
                defaultValue: false);

            migrationBuilder.CreateTable(
                name: "ErrorReportReason",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ErrorReportReason", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ErrorReport",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Description = table.Column<string>(nullable: true),
                    Ticket = table.Column<int>(nullable: false),
                    UserId = table.Column<int>(nullable: true),
                    ErrorReportReasonId = table.Column<int>(nullable: true),
                    Date = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ErrorReport", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ErrorReport_ErrorReportReason_ErrorReportReasonId",
                        column: x => x.ErrorReportReasonId,
                        principalTable: "ErrorReportReason",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ErrorReport_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 156,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 233,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 272,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 393,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 1521,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 2235,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 2750,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 2852,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 3455,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 3778,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 3832,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4024,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4143,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4275,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4437,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4459,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4770,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4780,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4834,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4911,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5115,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5116,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5129,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5230,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5320,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5386,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5389,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5399,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5404,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5405,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5409,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5413,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5419,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5420,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5423,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5442,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5443,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5448,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5449,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5456,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5464,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5471,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5474,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5487,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5488,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5498,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5509,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5530,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5531,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5533,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5543,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5565,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5567,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5569,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5572,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5573,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5576,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5577,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5586,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5595,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5599,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5623,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5656,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5659,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5661,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5668,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5693,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5712,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5714,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5715,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5722,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5733,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5739,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5746,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5749,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5756,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5761,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5765,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5769,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5776,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5791,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5795,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5798,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5810,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5820,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5833,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5850,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5856,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5863,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5865,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5869,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5882,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5910,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5923,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5924,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5954,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5966,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5972,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5976,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5985,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6009,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6022,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6024,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6027,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6036,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6048,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6067,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6070,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6076,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6095,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6096,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6103,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6104,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6108,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6121,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6129,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6139,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6141,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6143,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6144,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6154,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6158,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6162,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6171,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6178,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6182,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6187,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6195,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6201,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6207,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6209,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6216,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6233,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6235,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6241,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6246,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6261,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6272,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6276,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6277,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6279,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6282,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6322,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6379,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6506,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6749,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6851,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7058,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7154,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7163,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7203,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7213,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7458,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7601,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7614,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7817,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7950,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8398,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8471,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8493,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8756,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8837,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8838,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 9112,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 55298,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 71242,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 76066,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 88412,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 88579,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 89710,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 89897,
                column: "EnabledInteration",
                value: true);

            migrationBuilder.CreateIndex(
                name: "IX_ErrorReport_ErrorReportReasonId",
                table: "ErrorReport",
                column: "ErrorReportReasonId");

            migrationBuilder.CreateIndex(
                name: "IX_ErrorReport_UserId",
                table: "ErrorReport",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ErrorReport");

            migrationBuilder.DropTable(
                name: "ErrorReportReason");

            migrationBuilder.DropColumn(
                name: "General",
                table: "SystemAlert");

            migrationBuilder.CreateTable(
                name: "UserErrorNotification",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Date = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    UserId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserErrorNotification", x => x.Id);
                    table.ForeignKey(
                        name: "FK_UserErrorNotification_User_UserId",
                        column: x => x.UserId,
                        principalTable: "User",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 156,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 233,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 272,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 393,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 1521,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 2235,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 2750,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 2852,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 3455,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 3778,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 3832,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4024,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4143,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4275,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4437,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4459,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4770,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4780,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4834,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4911,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5115,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5116,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5129,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5230,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5320,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5386,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5389,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5399,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5404,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5405,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5409,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5413,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5419,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5420,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5423,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5442,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5443,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5448,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5449,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5456,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5464,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5471,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5474,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5487,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5488,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5498,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5509,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5530,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5531,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5533,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5543,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5565,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5567,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5569,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5572,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5573,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5576,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5577,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5586,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5595,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5599,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5623,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5656,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5659,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5661,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5668,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5693,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5712,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5714,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5715,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5722,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5733,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5739,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5746,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5749,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5756,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5761,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5765,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5769,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5776,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5791,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5795,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5798,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5810,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5820,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5833,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5850,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5856,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5863,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5865,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5869,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5882,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5910,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5923,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5924,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5954,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5966,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5972,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5976,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5985,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6009,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6022,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6024,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6027,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6036,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6048,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6067,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6070,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6076,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6095,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6096,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6103,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6104,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6108,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6121,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6129,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6139,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6141,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6143,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6144,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6154,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6158,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6162,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6171,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6178,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6182,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6187,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6195,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6201,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6207,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6209,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6216,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6233,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6235,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6241,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6246,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6261,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6272,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6276,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6277,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6279,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6282,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6322,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6379,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6506,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6749,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6851,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7058,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7154,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7163,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7203,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7213,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7458,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7601,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7614,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7817,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7950,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8398,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8471,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8493,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8756,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8837,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8838,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 9112,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 55298,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 71242,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 76066,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 88412,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 88579,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 89710,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 89897,
                column: "EnabledInteration",
                value: false);

            migrationBuilder.CreateIndex(
                name: "IX_UserErrorNotification_UserId",
                table: "UserErrorNotification",
                column: "UserId");
        }
    }
}
