﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace COPMDU.Infrastructure.InterationException
{


    [Serializable]
    public class PageNotFoundException : Exception
    {
        public PageNotFoundException(string url) : base($"Erro 404 página não encontrada '{url}'") { }
    }
}
