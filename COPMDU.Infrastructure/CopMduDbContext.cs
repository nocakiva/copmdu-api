﻿using System;
using System.Security.Cryptography;
using COPMDU.Domain;
using COPMDU.Domain.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace COPMDU.Infrastructure
{
    public partial class CopMduDbContext : DbContext
    {

        #region DbSets
        public DbSet<City> City { get; set; }
        public DbSet<ExternalSystem> ExternalSystem { get; set; }
        public DbSet<Resolution> Resolution { get; set; }
        public DbSet<Outage> Outage { get; set; }
        public DbSet<User> User { get; set; }
        public DbSet<Username> Username { get; set; }
        public DbSet<OutageSymptom> OutageSymptom { get; set; }
        public DbSet<OutageType> OutageType { get; set; }
        public DbSet<ServiceOrderType> ServiceOrderType { get; set; }
        public DbSet<SignalStatus> SignalStatus { get; set; }
        public DbSet<SignalTerminal> SignalTerminal { get; set; }
        public DbSet<Log> Log { get; set; }
        public DbSet<WebCommLog> WebCommLog { get; set; }
        public DbSet<Service> Service { get; set; }
        public DbSet<OutageService> OutageService { get; set; }
        public DbSet<SystemAlert> SystemAlert { get; set; }
        public DbSet<ErrorReport> ErrorReport { get; set; }
        public DbSet<ErrorReportReason> ErrorReportReason { get; set; }
        #endregion

        public CopMduDbContext(DbContextOptions<CopMduDbContext> options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region Table association
            modelBuilder.Entity<OutageSignal>()
                .HasMany(os => os.CmRx)
                .WithOne(v => v.OutageSignal)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<OutageSignal>()
                .HasMany(os => os.CmSnr)
                .WithOne(v => v.OutageSignal)
                .OnDelete(DeleteBehavior.Cascade);
            #endregion

            #region Table Primary Key
            modelBuilder.Entity<OutageService>()
                .HasKey(x => new { x.OutageId, x.ServiceId });
            #endregion

            #region Seed Data
            modelBuilder.Entity<Service>().HasData(
                new Service { Id = 1, Name = "NET VIRTUA" },
                new Service { Id = 2, Name = "NET FONE" },
                new Service { Id = 4, Name = "Pay TV - Digital" },
                new Service { Id = 6, Name = "Pay TV - Analógico" },
                new Service { Id = 16, Name = "NOW" },
                new Service { Id = 17, Name = "BSOD" },
                new Service { Id = 18, Name = "WiFi" },
                new Service { Id = 19, Name = "AVV" },
                new Service { Id = 20, Name = "NOW Online" },
                new Service { Id = 21, Name = "TV Everywhere" }
                );
            modelBuilder.Entity<SignalStatus>().HasData(
                new SignalStatus { Id = 1, Name = "Online" },
                new SignalStatus { Id = 2, Name = "Offline" },
                new SignalStatus { Id = 3, Name = "NOT FOUND" },
                new SignalStatus { Id = 4, Name = "RangingAutoAdjComplete" },
                new SignalStatus { Id = 10, Name = "Failed" }
                );
            modelBuilder.Entity<SignalTerminal>().HasData(
                new SignalTerminal { Id = 1, Name = "DECODER DIGITAL" },
                new SignalTerminal { Id = 2, Name = "EMTA" }
                );
            modelBuilder.Entity<ServiceOrderType>().HasData(
                new ServiceOrderType { Id = 114, Name = "114" },
                new ServiceOrderType { Id = 115, Name = "115" }
                );
            modelBuilder.Entity<OutageSymptom>().HasData(
                new OutageSymptom { Id = 1, Name = "INFORMATIVO" },
                new OutageSymptom { Id = 2, Name = "DEGRADAÇÃO" },
                new OutageSymptom { Id = 3, Name = "SEM SINAL" },
                new OutageSymptom { Id = 4, Name = "INTERMITENTE" }
                );
            modelBuilder.Entity<User>().HasData(
                new User { Id = 1, Name = "Robo Serena", Username = "serena", Password = "serena@2019", Email = "v.arre@akivasoftware.com.br", CityId = 71986, Phone = 999999999, Prefix = 51, ChangePassword = false }
                );
            modelBuilder.Entity<ExternalSystem>().HasData(
                new ExternalSystem { Id = 1, Name = "netsul" },
                new ExternalSystem { Id = 2, Name = "sigabc" },
                new ExternalSystem { Id = 3, Name = "sigmaisp" },
                new ExternalSystem { Id = 4, Name = "sigsantos" },
                new ExternalSystem { Id = 5, Name = "sigsoc" },
                new ExternalSystem { Id = 6, Name = "sigspo" },
                new ExternalSystem { Id = 7, Name = "netbh" }
                );
            modelBuilder.Entity<Resolution>().HasData(
                new Resolution { Id = 2959, Description = "EQUALIZAÇÃO DE AMPLIFICADOR", AtlasId = 7048, AtlasDescription = "EQUALIZACAO DO AMPLIFICADOR", Active = true },
                new Resolution { Id = 2960, Description = "LIMPEZA DE RUÍDO", AtlasId = 7050, AtlasDescription = "LIMPEZA DE RUÍDO NO MDU", Active = false },
                new Resolution { Id = 2961, Description = "MANUTENÇÃO NO AMPLIFICADOR", AtlasId = 7064, AtlasDescription = "ADEQUAÇÃO / CONSTRUÇÃO DE MDU", Active = false },
                new Resolution { Id = 2962, Description = "MANUTENÇÃO NO DG", AtlasId = 7064, AtlasDescription = "ADEQUAÇÃO / CONSTRUÇÃO DE MDU", Active = false },
                new Resolution { Id = 2963, Description = "QUEDA DE ENERGIA - ACIONADO GERADOR", AtlasId = 7067, AtlasDescription = "ALTERAÇÃO DE ALIMENTAÇÃO ELÉTRICA DO INDOOR", Active = false },
                new Resolution { Id = 2964, Description = "QUEDA DE ENERGIA - FORNECIMENTO REESTABELECIDO", AtlasId = 7067, AtlasDescription = "ALTERAÇÃO DE ALIMENTAÇÃO ELÉTRICA DO INDOOR", Active = false },
                new Resolution { Id = 2965, Description = "REFEITA CONEXÃO - BACK BONE", AtlasId = 7051, AtlasDescription = "REFEITA CONEXAO DO BACKBONE", Active = true },
                new Resolution { Id = 2966, Description = "RECONSTRUÇÃO DE MDU", AtlasId = 7064, AtlasDescription = "ADEQUAÇÃO / CONSTRUÇÃO DE MDU", Active = false },
                new Resolution { Id = 2967, Description = "TROCA DE AMPLIFICADOR", AtlasId = 7047, AtlasDescription = "AMPLIFICADOR INDOOR COM DEFEITO - EMERGÊNCIA", Active = true },
                new Resolution { Id = 2968, Description = "TROCA DE DROP", AtlasId = 7045, AtlasDescription = "ROMPIMENTO DO CABO DE BACKBONE", Active = true },
                new Resolution { Id = 3061, Description = "DISJUNTOR DE SERVIÇO DO CONDOMÍNIO DESLIGADO", AtlasId = 7067, AtlasDescription = "ALTERAÇÃO DE ALIMENTAÇÃO ELÉTRICA DO INDOOR", Active = false },
                new Resolution { Id = 3062, Description = "ENERGIA ELÉTRICA DO CONDOMÍNIO CORTADA PELA CONCESSIONÁRIA", AtlasId = 7046, AtlasDescription = "ALIMENTACAO INTERNA(PROBLEMA DE AC)", Active = false },
                new Resolution { Id = 3063, Description = "PASSIVO DANIFICADO", AtlasId = 7060, AtlasDescription = "PASSIVO DE BACKBONE DANIFICADO", Active = true },
                new Resolution { Id = 3064, Description = "REFEITA CONEXÃO DO CABO RG11 NO TAP", AtlasId = 7051, AtlasDescription = "REFEITA CONEXAO DO BACKBONE", Active = true },
                new Resolution { Id = 3067, Description = "ACESSO PROIBIDO AO PONTO DE FALHA", AtlasId = 7070, AtlasDescription = "OCORRENCIA CANCELADA", Active = false },
                new Resolution { Id = 5403, Description = "DISJUNTOR DO MDU DESARMADO", AtlasId = 7067, AtlasDescription = "ALTERAÇÃO DE ALIMENTAÇÃO ELÉTRICA DO INDOOR", Active = true },
                new Resolution { Id = 5404, Description = "AMPLIFICADOR QUEIMADO", AtlasId = 7047, AtlasDescription = "AMPLIFICADOR INDOOR COM DEFEITO - EMERGÊNCIA", Active = true },
                new Resolution { Id = 5405, Description = "BOOT NO AMPLIFICADOR", AtlasId = 7047, AtlasDescription = "AMPLIFICADOR INDOOR COM DEFEITO - EMERGÊNCIA", Active = false },
                new Resolution { Id = 5406, Description = "FURTO DE ATIVO", AtlasId = 7053, AtlasDescription = "FURTO DE AMPLIFICADOR INDOOR", Active = false },
                new Resolution { Id = 5423, Description = "NORMALIZADO SEM INTERVENCAO", AtlasId = 7059, AtlasDescription = "PROBLEMA INTERMITENTE DE BACKBONE NAO CONSTATADO", Active = true },
                new Resolution { Id = 5424, Description = "ERRO DE ABERTURA", AtlasId = 7070, AtlasDescription = "OCORRENCIA CANCELADA", Active = false },
                new Resolution { Id = 5425, Description = "REDE EXTERNA COM PROBLEMA", AtlasId = 7062, AtlasDescription = "REPASSADO PARA REDE EXTERNA", Active = false }
                );
            modelBuilder.Entity<City>().HasData(
                new City { Id = 22, Name = "Porto Velho", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 220 },
                new City { Id = 66, Name = "Rio Branco", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 66 },
                new City { Id = 121, Name = "Manaus", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 121 },
                new City { Id = 156, Name = "Boa Vista", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 0 },
                new City { Id = 178, Name = "Ananindeua", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 178 },
                new City { Id = 194, Name = "Belém", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 194 },
                new City { Id = 233, Name = "Castanhal", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 23 },
                new City { Id = 272, Name = "Maraba", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 2 },
                new City { Id = 393, Name = "Macapa", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 393 },
                new City { Id = 540, Name = "Palmas", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 540 },
                new City { Id = 696, Name = "São Luís", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 96 },
                new City { Id = 863, Name = "Teresina", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 63 },
                new City { Id = 1097, Name = "Fortaleza", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 97 },
                new City { Id = 1521, Name = "Sobral", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 521 },
                new City { Id = 1637, Name = "Parnamirim", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 637 },
                new City { Id = 1695, Name = "Natal", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 95 },
                new City { Id = 1833, Name = "Cabedelo", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 133 },
                new City { Id = 1847, Name = "Campina Grande", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 847 },
                new City { Id = 1907, Name = "João Pessoa", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 907 },
                new City { Id = 2152, Name = "Caruaru", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 152 },
                new City { Id = 2235, Name = "Jaboatão dos Guararapes", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 223 },
                new City { Id = 2269, Name = "Olinda", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 269 },
                new City { Id = 2291, Name = "Paulista", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 291 },
                new City { Id = 2480, Name = "Maceió", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 480 },
                new City { Id = 2550, Name = "Aracajú", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 550 },
                new City { Id = 2750, Name = "Camaçari", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 750 },
                new City { Id = 2852, Name = "Feira de Santana", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 0 },
                new City { Id = 3042, Name = "Lauro de Freitas", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 42 },
                new City { Id = 3230, Name = "Salvador", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 230 },
                new City { Id = 3363, Name = "Vitória da Conquista", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 336 },
                new City { Id = 3455, Name = "Araguari", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 455 },
                new City { Id = 3514, Name = "Betim", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 514 },
                new City { Id = 3741, Name = "Conselheiro Lafaiete", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 741 },
                new City { Id = 3753, Name = "Contagem", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 753 },
                new City { Id = 3778, Name = "Coronel Fabriciano", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 778 },
                new City { Id = 3832, Name = "Divinópolis", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 832 },
                new City { Id = 3930, Name = "Governador Valadares", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 930 },
                new City { Id = 4005, Name = "Ipatinga", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 405 },
                new City { Id = 4024, Name = "Itajubá", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 402 },
                new City { Id = 4060, Name = "Ituiutaba", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 60 },
                new City { Id = 4126, Name = "Juiz de Fora", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 126 },
                new City { Id = 4143, Name = "Lagoa Santa", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 0 },
                new City { Id = 4275, Name = "Montes Claros", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 275 },
                new City { Id = 4312, Name = "Nova Lima", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 312 },
                new City { Id = 4437, Name = "Poços de Caldas", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 37 },
                new City { Id = 4459, Name = "Pouso Alegre", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 459 },
                new City { Id = 4541, Name = "Sabará", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 541 },
                new City { Id = 4742, Name = "Sete Lagoas", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 742 },
                new City { Id = 4763, Name = "Teófilo Otoni", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 763 },
                new City { Id = 4770, Name = "Timóteo", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 477 },
                new City { Id = 4780, Name = "Três Corações", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 780 },
                new City { Id = 4803, Name = "Uberaba", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 803 },
                new City { Id = 4806, Name = "Uberlândia", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 806 },
                new City { Id = 4821, Name = "Varginha", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 821 },
                new City { Id = 4834, Name = "Vespasiano", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 834 },
                new City { Id = 4911, Name = "Cachoeiro de Itapemirim", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 911 },
                new City { Id = 4917, Name = "Cariacica", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 917 },
                new City { Id = 5066, Name = "Serra", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 566 },
                new City { Id = 5078, Name = "Vila Velha", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 507 },
                new City { Id = 5083, Name = "Vitória", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 508 },
                new City { Id = 5100, Name = "Barra Mansa", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 100 },
                new City { Id = 5105, Name = "Belford Roxo", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 105 },
                new City { Id = 5115, Name = "Cabo Frio", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 115 },
                new City { Id = 5116, Name = "Armação dos Búzios", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 116 },
                new City { Id = 5129, Name = "Campos dos Goytacazes", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 529 },
                new City { Id = 5161, Name = "Duque de Caxias", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 161 },
                new City { Id = 5197, Name = "Macaé", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 197 },
                new City { Id = 5226, Name = "Nilópolis", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 226 },
                new City { Id = 5228, Name = "Niterói", ExternalSystemId = 4, EnabledInteration = true, OperatorId = 228 },
                new City { Id = 5230, Name = "Nova Friburgo", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 30 },
                new City { Id = 5237, Name = "Nova Iguaçu", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 237 },
                new City { Id = 5239, Name = "Mesquita", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 239 },
                new City { Id = 5250, Name = "Petrópolis", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 250 },
                new City { Id = 5268, Name = "Resende", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 268 },
                new City { Id = 5285, Name = "Rio das Ostras", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 285 },
                new City { Id = 5305, Name = "São Gonçalo", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 305 },
                new City { Id = 5316, Name = "São João de Meriti", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 316 },
                new City { Id = 5320, Name = "São Pedro da Aldeia", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 320 },
                new City { Id = 5338, Name = "Teresópolis", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 338 },
                new City { Id = 5359, Name = "Volta Redonda", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 359 },
                new City { Id = 5386, Name = "Americana", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 386 },
                new City { Id = 5389, Name = "Amparo", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 538 },
                new City { Id = 5399, Name = "Aparecida", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 399 },
                new City { Id = 5404, Name = "Araçatuba", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 404 },
                new City { Id = 5405, Name = "Aracoiaba da Serra", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 0 },
                new City { Id = 5409, Name = "Araraquara", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 409 },
                new City { Id = 5413, Name = "Araras", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 413 },
                new City { Id = 5419, Name = "Artur Nogueira", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 419 },
                new City { Id = 5420, Name = "Arujá", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 420 },
                new City { Id = 5423, Name = "Atibaia", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 423 },
                new City { Id = 5442, Name = "Barrinha", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 442 },
                new City { Id = 5443, Name = "Barueri", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 443 },
                new City { Id = 5448, Name = "Batatais", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 448 },
                new City { Id = 5449, Name = "Bauru", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 8 },
                new City { Id = 5456, Name = "Bertioga", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 456 },
                new City { Id = 5464, Name = "Boituva", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 464 },
                new City { Id = 5471, Name = "Botucatu", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 471 },
                new City { Id = 5474, Name = "Bragança Paulista", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 474 },
                new City { Id = 5487, Name = "Caçapava", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 487 },
                new City { Id = 5488, Name = "Cachoeira Paulista", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 488 },
                new City { Id = 5498, Name = "Caieiras", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 498 },
                new City { Id = 5509, Name = "Campinas", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 52 },
                new City { Id = 5530, Name = "Capivari", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 530 },
                new City { Id = 5531, Name = "Caraguatatuba", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 553 },
                new City { Id = 5533, Name = "Carapicuíba", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 533 },
                new City { Id = 5543, Name = "Catanduva", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 543 },
                new City { Id = 5565, Name = "Cosmópolis", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 565 },
                new City { Id = 5567, Name = "Cotia", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 567 },
                new City { Id = 5569, Name = "Cravinhos", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 569 },
                new City { Id = 5572, Name = "Cruzeiro", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 572 },
                new City { Id = 5573, Name = "Cubatão", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 573 },
                new City { Id = 5576, Name = "Descalvado", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 576 },
                new City { Id = 5577, Name = "Diadema", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 577 },
                new City { Id = 5586, Name = "Dracena", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 586 },
                new City { Id = 5595, Name = "Elias Fausto", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 595 },
                new City { Id = 5599, Name = "Embu das Artes", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 599 },
                new City { Id = 5623, Name = "Franca", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 56 },
                new City { Id = 5656, Name = "Guaratinguetá", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 656 },
                new City { Id = 5659, Name = "Guarujá", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 659 },
                new City { Id = 5661, Name = "Guarulhos", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 661 },
                new City { Id = 5668, Name = "Hortolândia", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 668 },
                new City { Id = 5693, Name = "Indaiatuba", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 54 },
                new City { Id = 5712, Name = "Itanhaém", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 712 },
                new City { Id = 5714, Name = "Itapecerica da Serra", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 714 },
                new City { Id = 5715, Name = "Itapetininga", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 715 },
                new City { Id = 5722, Name = "Itapevi", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 722 },
                new City { Id = 5733, Name = "Itaquaquecetuba", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 0 },
                new City { Id = 5739, Name = "Itatiba", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 739 },
                new City { Id = 5746, Name = "Itú", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 746 },
                new City { Id = 5749, Name = "Itúverava", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 574 },
                new City { Id = 5756, Name = "Jacareí", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 756 },
                new City { Id = 5761, Name = "Jaguariúna", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 761 },
                new City { Id = 5765, Name = "Jandira", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 765 },
                new City { Id = 5769, Name = "Jaú", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 769 },
                new City { Id = 5776, Name = "Jundiaí", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 55 },
                new City { Id = 5791, Name = "Limeira", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 791 },
                new City { Id = 5795, Name = "Lorena", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 795 },
                new City { Id = 5798, Name = "Louveira", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 798 },
                new City { Id = 5810, Name = "Mairinque", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 810 },
                new City { Id = 5820, Name = "Marília", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 820 },
                new City { Id = 5833, Name = "Mauá", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 833 },
                new City { Id = 5850, Name = "Mirassol", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 850 },
                new City { Id = 5856, Name = "Mogi das Cruzes", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 856 },
                new City { Id = 5863, Name = "Mogi Guaçu", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 863 },
                new City { Id = 5865, Name = "Mogi Mirim", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 865 },
                new City { Id = 5869, Name = "Mongaguá", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 869 },
                new City { Id = 5882, Name = "Monte Mor", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 882 },
                new City { Id = 5910, Name = "Nova Odessa", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 910 },
                new City { Id = 5923, Name = "Orlândia", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 592 },
                new City { Id = 5924, Name = "Osasco", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 924 },
                new City { Id = 5954, Name = "Paulínia", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 954 },
                new City { Id = 5966, Name = "Pedreira", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 966 },
                new City { Id = 5972, Name = "Peruíbe", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 972 },
                new City { Id = 5976, Name = "Pindamonhangaba", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 976 },
                new City { Id = 5985, Name = "Piracicaba", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 9 },
                new City { Id = 6009, Name = "Poá", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 609 },
                new City { Id = 6022, Name = "Porto Feliz", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 22 },
                new City { Id = 6024, Name = "Potim", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 24 },
                new City { Id = 6027, Name = "Praia Grande", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 27 },
                new City { Id = 6036, Name = "Presidente Prudente", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 36 },
                new City { Id = 6048, Name = "Rafard", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 48 },
                new City { Id = 6067, Name = "Ribeirão Pires", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 67 },
                new City { Id = 6070, Name = "Ribeirão Preto", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 5 },
                new City { Id = 6076, Name = "Rio Claro", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 76 },
                new City { Id = 6095, Name = "Salto", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 295 },
                new City { Id = 6096, Name = "Salto de Pirapora", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 0 },
                new City { Id = 6103, Name = "Santa Bárbara d Oeste", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 103 },
                new City { Id = 6104, Name = "Santa Branca", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 0 },
                new City { Id = 6108, Name = "Santa Cruz do Rio Pardo", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 108 },
                new City { Id = 6121, Name = "Santana do Parnaíba", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 621 },
                new City { Id = 6129, Name = "Santo André", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 129 },
                new City { Id = 6139, Name = "Santos", ExternalSystemId = 4, EnabledInteration = true, OperatorId = 4 },
                new City { Id = 6141, Name = "São Bernardo do Campo", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 141 },
                new City { Id = 6143, Name = "São Caetano do Sul", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 143 },
                new City { Id = 6144, Name = "São Carlos", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 53 },
                new City { Id = 6154, Name = "São Joaquim da Barra", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 615 },
                new City { Id = 6158, Name = "São José do Rio Preto", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 6 },
                new City { Id = 6162, Name = "São José dos Campos", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 162 },
                new City { Id = 6171, Name = "São Miguel Arcanjo", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 0 },
                new City { Id = 6178, Name = "São Roque", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 678 },
                new City { Id = 6182, Name = "São Sebastião", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 182 },
                new City { Id = 6187, Name = "São Vicente", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 187 },
                new City { Id = 6195, Name = "Sertãozinho", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 195 },
                new City { Id = 6201, Name = "Sorocaba", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 7 },
                new City { Id = 6207, Name = "Sumaré", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 207 },
                new City { Id = 6209, Name = "Suzano", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 209 },
                new City { Id = 6216, Name = "Taboão da Serra", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 216 },
                new City { Id = 6233, Name = "Tatuí", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 233 },
                new City { Id = 6235, Name = "Taubaté", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 235 },
                new City { Id = 6241, Name = "Tietê", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 241 },
                new City { Id = 6246, Name = "Tremembé", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 246 },
                new City { Id = 6261, Name = "Ubatuba", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 261 },
                new City { Id = 6272, Name = "Valinhos", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 272 },
                new City { Id = 6276, Name = "Vargem Grande Paulista", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 276 },
                new City { Id = 6277, Name = "Várzea Paulista", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 277 },
                new City { Id = 6279, Name = "Vinhedo", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 279 },
                new City { Id = 6282, Name = "Votorantim", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 282 },
                new City { Id = 6294, Name = "Almirante Tamandaré", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 294 },
                new City { Id = 6322, Name = "Apucarana", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 322 },
                new City { Id = 6333, Name = "Araucária", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 333 },
                new City { Id = 6379, Name = "Cambé", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 379 },
                new City { Id = 6389, Name = "Campo Largo", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 389 },
                new City { Id = 6413, Name = "Cascavel", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 641 },
                new City { Id = 6430, Name = "Cianorte", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 430 },
                new City { Id = 6437, Name = "Colombo", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 437 },
                new City { Id = 6506, Name = "Foz do Iguaçu", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 0 },
                new City { Id = 6531, Name = "Guarapuava", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 531 },
                new City { Id = 6749, Name = "Paranaguá", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 749 },
                new City { Id = 6770, Name = "Pinhais", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 770 },
                new City { Id = 6794, Name = "Ponta Grossa", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 794 },
                new City { Id = 6851, Name = "Rolândia", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 851 },
                new City { Id = 6923, Name = "São José dos Pinhais", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 923 },
                new City { Id = 7052, Name = "Balneário Camboriú", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 752 },
                new City { Id = 7058, Name = "Biguaçu", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 58 },
                new City { Id = 7070, Name = "Brusque", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 70 },
                new City { Id = 7154, Name = "Gaspar", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 0 },
                new City { Id = 7163, Name = "Guaramirim", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 163 },
                new City { Id = 7193, Name = "Itajaí", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 193 },
                new City { Id = 7194, Name = "Itapema", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 94 },
                new City { Id = 7203, Name = "Jaraguá do Sul", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 203 },
                new City { Id = 7213, Name = "Lages", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 213 },
                new City { Id = 7260, Name = "Navegantes", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 260 },
                new City { Id = 7278, Name = "Palhoça", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 278 },
                new City { Id = 7363, Name = "São José", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 363 },
                new City { Id = 7458, Name = "Alvorada", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 458 },
                new City { Id = 7560, Name = "Cachoeirinha", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 560 },
                new City { Id = 7584, Name = "Campo Bom", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 584 },
                new City { Id = 7601, Name = "Canela", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 601 },
                new City { Id = 7603, Name = "Canoas", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 603 },
                new City { Id = 7614, Name = "Carazinho", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 614 },
                new City { Id = 7757, Name = "Estância Velha", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 757 },
                new City { Id = 7758, Name = "Esteio", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 758 },
                new City { Id = 7817, Name = "Gramado", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 817 },
                new City { Id = 7820, Name = "Gravataí", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 720 },
                new City { Id = 7950, Name = "Montenegro", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 950 },
                new City { Id = 8330, Name = "Sapiranga", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 330 },
                new City { Id = 8332, Name = "Sapucaia do Sul", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 332 },
                new City { Id = 8398, Name = "Torres", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 398 },
                new City { Id = 8471, Name = "Viamão", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 871 },
                new City { Id = 8493, Name = "Xangri-lá", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 0 },
                new City { Id = 8533, Name = "Campo Grande", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 11 },
                new City { Id = 8567, Name = "Dourados", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 867 },
                new City { Id = 8719, Name = "Cuiabá", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 719 },
                new City { Id = 8756, Name = "Lucas do Rio Verde", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 875 },
                new City { Id = 8818, Name = "Rondonópolis", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 818 },
                new City { Id = 8837, Name = "Sinop", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 837 },
                new City { Id = 8838, Name = "Sorriso", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 838 },
                new City { Id = 8858, Name = "Várzea Grande", ExternalSystemId = 5, EnabledInteration = true, OperatorId = 858 },
                new City { Id = 8895, Name = "Aparecida de Goiânia", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 895 },
                new City { Id = 9112, Name = "Rio Verde", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 112 },
                new City { Id = 15890, Name = "Brasília", ExternalSystemId = 4, EnabledInteration = true, OperatorId = 40 },
                new City { Id = 18511, Name = "Anápolis", ExternalSystemId = 4, EnabledInteration = true, OperatorId = 15 },
                new City { Id = 19887, Name = "Goiânia", ExternalSystemId = 4, EnabledInteration = true, OperatorId = 10 },
                new City { Id = 25666, Name = "Belo Horizonte", ExternalSystemId = 7, EnabledInteration = true, OperatorId = 13 },
                new City { Id = 51136, Name = "Recife", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 136 },
                new City { Id = 53902, Name = "Arapongas", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 93 },
                new City { Id = 55298, Name = "Curitiba", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 884 },
                new City { Id = 56995, Name = "Londrina", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 996 },
                new City { Id = 57304, Name = "Maringá", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 91 },
                new City { Id = 63118, Name = "Rio de Janeiro", ExternalSystemId = 4, EnabledInteration = true, OperatorId = 38 },
                new City { Id = 66770, Name = "Bagé", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 690 },
                new City { Id = 67040, Name = "Bento Gonçalves", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 688 },
                new City { Id = 67784, Name = "Capão da Canoa", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 87 },
                new City { Id = 68020, Name = "Caxias do Sul", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 687 },
                new City { Id = 68659, Name = "Cruz Alta", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 685 },
                new City { Id = 69019, Name = "Erechim", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 695 },
                new City { Id = 69337, Name = "Farroupilha", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 83 },
                new City { Id = 70408, Name = "Lajeado", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 689 },
                new City { Id = 71242, Name = "Novo Hamburgo", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 686 },
                new City { Id = 71587, Name = "Passo Fundo", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 693 },
                new City { Id = 71706, Name = "Pelotas", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 691 },
                new City { Id = 71986, Name = "Porto Alegre", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 78 },
                new City { Id = 72451, Name = "Rio Grande", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 692 },
                new City { Id = 72842, Name = "Santa Cruz do Sul", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 694 },
                new City { Id = 72907, Name = "Santa Maria", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 75 },
                new City { Id = 74748, Name = "Uruguaiana", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 684 },
                new City { Id = 75680, Name = "Blumenau", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 700 },
                new City { Id = 75681, Name = "Blumenau Btv", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 0 },
                new City { Id = 76066, Name = "Chapecó", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 0 },
                new City { Id = 76180, Name = "Criciúma", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 89 },
                new City { Id = 76414, Name = "Florianópolis", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 88 },
                new City { Id = 77127, Name = "Joinville", ExternalSystemId = 1, EnabledInteration = true, OperatorId = 86 },
                new City { Id = 88412, Name = "São Paulo", ExternalSystemId = 6, EnabledInteration = true, OperatorId = 3 },
                new City { Id = 88579, Name = "Serra Negra", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 579 },
                new City { Id = 89710, Name = "São Leopoldo", ExternalSystemId = 3, EnabledInteration = true, OperatorId = 710 },
                new City { Id = 89897, Name = "Santos ABC", ExternalSystemId = 2, EnabledInteration = true, OperatorId = 897 }
                 );
            modelBuilder.Entity<ErrorReportReason>().HasData(
                new ErrorReportReason { Id = 1, Name = "Ticket não encontrado no APP" },
                new ErrorReportReason { Id = 2, Name = "Validação de sinais errada" },
                new ErrorReportReason { Id = 3, Name = "Erro na validação de sinais" },
                new ErrorReportReason { Id = 4, Name = "Aplicativo fecha sozinho" },
                new ErrorReportReason { Id = 5, Name = "Erro no fechamento do ticket" },
                new ErrorReportReason { Id = 6, Name = "Outros" }
                );

            #endregion
        }
    }
}